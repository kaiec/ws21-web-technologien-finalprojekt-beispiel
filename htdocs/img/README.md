# Bilderordner

Legt hier bitte alle Bilder rein, die von mehreren Unterseiten genutzt werden sollen.

Bilder, die man definitiv nur einmal braucht, kommen in separate Ordner auf der Ebene der HTML-Datei.

## Lizenzinfos

Hier für JEDES Bild die Infos zu Quelle und Lizenz hinterlegen!
